package ru.mail.park.listofnumbers;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NumberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private MainActivity activity;

    NumberListAdapter(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View cardView = LayoutInflater.from(context).inflate(R.layout.item_number, parent, false);
        return new NumberListAdapter.CardViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CardViewHolder cardHolder = ((CardViewHolder) holder);
        cardHolder.SetNumber(position);
    }

    @Override
    public int getItemCount() {
        return activity.getResources().getInteger(R.integer.numbers_count);
    }

    class CardViewHolder extends RecyclerView.ViewHolder {
        Integer value;
        TextView numberView;

        CardViewHolder(final View itemView) {
            super(itemView);
            numberView = itemView.findViewById(R.id.number);
            numberView.setOnClickListener(new NumberClick());
        }

        void SetNumber(Integer value) {
            this.value = value;
            numberView.setText(value.toString());
        }

        class NumberClick implements View.OnClickListener{
            @Override
            public void onClick(View view) {
                activity.openNumber(value);
            }
        }
    }
}
