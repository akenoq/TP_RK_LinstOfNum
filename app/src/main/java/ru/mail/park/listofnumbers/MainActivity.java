package ru.mail.park.listofnumbers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        Fragment main = getSupportFragmentManager().findFragmentById(R.id.main_container);

        if (main != null && main.isAdded()) {
            transaction.remove(main);
        }
        transaction.replace(R.id.main_container, new NumberListFragment());
        transaction.commit();
    }

    public void openNumber(Integer value) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        NumberFragment articleFragment = NumberFragment.newInstance(value * value, this);

        transaction.replace(R.id.main_container, articleFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
