package ru.mail.park.listofnumbers;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class NumberFragment extends Fragment {
    private Integer value;

    public static NumberFragment newInstance(Number number, Activity parent) {
        NumberFragment fragment = new NumberFragment();
        Bundle args = new Bundle();
        args.putInt(parent.getString(R.string.number_fragment_value), number.intValue());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        value = getArguments() != null ? getArguments().getInt(getString(R.string.number_fragment_value)) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_number, container, false);

        TextView textView = view.findViewById(R.id.number);
        textView.setText(value.toString());

        return view;
    }
}
