package ru.mail.park.listofnumbers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class NumberListFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_numbers_list, container, false);
        MainActivity mainActivity = (MainActivity) getActivity();

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        NumberListAdapter adapter = new NumberListAdapter(mainActivity);
        recyclerView.setAdapter(adapter);

        GridLayoutManager layoutManager = new GridLayoutManager(mainActivity, getColumnCount());
        recyclerView.setLayoutManager(layoutManager);

        return view;
    }

    private int getColumnCount() {
        return getResources().getInteger(R.integer.column_count);
    }
}
